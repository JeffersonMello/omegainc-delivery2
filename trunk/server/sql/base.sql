CREATE TABLE cad_imagens (
  guid INT NOT NULL AUTO_INCREMENT,
  diretorio VARCHAR(750) NOT NULL,
  PRIMARY KEY (guid)
);

CREATE TABLE cad_usuarios (
  guid INT NOT NULL AUTO_INCREMENT,
  usuario VARCHAR(250) NOT NULL,
  senha VARCHAR(32) NOT NULL,
  nivel INT NOT NULL DEFAULT 0,
  guid_imagem INT NOT NULL DEFAULT 0,
  PRIMARY KEY (guid),
  FOREIGN KEY (guid_imagem) REFERENCES cad_imagens (guid)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE cad_clientes (
  guid INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(750),
  email VARCHAR(750),
  senha VARCHAR(32),
  cpf VARCHAR(32),
  telefone VARCHAR(40),
  PRIMARY KEY (guid)
);

CREATE TABLE cad_pagamentos (
  guid INT NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(255) NOT NULL,
  PRIMARY KEY (guid)
);

CREATE TABLE cad_empresa (
  guid INT NOT NULL AUTO_INCREMENT,
  razaosocial VARCHAR(750) NOT NULL,
  nomefantasia VARCHAR(750) NOT NULL,
  cnpj VARCHAR(20) NOT NULL,
  inscricaoestadual VARCHAR(20) NOT NULL,
  endereco VARCHAR(750),
  horarioaatd VARCHAR(750),
  email VARCHAR(250),
  telefone VARCHAR(250),
  padrao INT NOT NULL DEFAULT 0,
  aberto INT NOT NULL DEFAULT 0,
  guid_imagem INT NOT NULL,
  PRIMARY KEY (guid),
  FOREIGN KEY (guid_imagem) REFERENCES cad_imagens (guid)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE cad_tamanhos (
  guid INT NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(250),
  precoadd FLOAT,
  PRIMARY KEY (guid)
);

CREATE TABLE lis_tamanhos (
  guid INT NOT NULL AUTO_INCREMENT,
  guid_tamanho INT NOT NULL,
  PRIMARY KEY (guid),
  FOREIGN KEY (guid_tamanho) REFERENCES cad_tamanhos (guid)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE cad_categorias (
  guid INT NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(750) NOT NULL,
  meioameio INT NOT NULL DEFAULT 0,
  icone VARCHAR(250) DEFAULT '<i class="fa fa-archive color-icon" aria-hidden="true"></i>',
  PRIMARY KEY (guid)
)

CREATE TABLE cad_adicionais (
  guid INT NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(750) NOT NULL,
  preco FLOAT,
  PRIMARY KEY (guid)
);

CREATE TABLE cad_produtos (
  guid INT NOT NULL AUTO_INCREMENT,
  guid_categoria INT NOT NULL,
  guid_listamanhos INT,
  guid_imagem INT NOT NULL,
  descricao VARCHAR(750) NOT NULL,
  subdescricao VARCHAR(850),
  indisponivel INT NOT NULL DEFAULT 0,
  meioameio INT NOT NULL DEFAULT 0,
  definetamanho INT NOT NULL DEFAULT 0,
  permadicionais INT NOT NULL DEFAULT 0,
  preco FLOAT,
  PRIMARY KEY (guid),
  FOREIGN KEY (guid_categoria) REFERENCES cad_categorias (guid)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  FOREIGN KEY (guid_listamanhos) REFERENCES lis_tamanhos (guid)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  FOREIGN KEY (guid_imagem) REFERENCES cad_imagens (guid)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE temp_prods (
  guid INT NOT NULL AUTO_INCREMENT,
  guid_imagem INT NOT NULL,
  descricao VARCHAR(750) NOT NULL,
  preco FLOAT,
  PRIMARY KEY (guid),
  FOREIGN KEY (guid_imagem) REFERENCES cad_imagens (guid)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE lis_lancprod (
  guid INT NOT NULL AUTO_INCREMENT,
  guid_produto INT NOT NULL,
  guid_pedido INT NOT NULL,
  valor_produto FLOAT,
  nome_prod VARCHAR(850),
  PRIMARY KEY (guid),
  FOREIGN KEY (guid_produto) REFERENCES cad_produtos (guid)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  FOREIGN KEY (guid_pedido) REFERENCES lanc_pedido (guid)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE lanc_pagamentos (
  guid INT NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(750) NOT NULL,
  token VARCHAR(850) NOT NULL,
  status INT NOT NULL,
  PRIMARY KEY (guid)
);


CREATE TABLE lanc_pedido (
  guid INT NOT NULL AUTO_INCREMENT,
  eguid VARCHAR(32),
  onesignalId VARCHAR(150),
  guid_cliente INT NOT NULL,
  token VARCHAR(32) NOT NULL,
  total FLOAT,
  local VARCHAR(450) NOT NULL,
  endereco VARCHAR(850) NOT NULL,
  numero VARCHAR(15) NOT NULL,
  datapedido DATETIME,
  entregar INT NOT NULL,
  guid_lancpagamento INT,
  guid_pgto INT NOT NULL,
  status INT NOT NULL,
  troco VARCHAR(250),
  observacao TEXT,
  notificado INT NOT NULL DEFAULT 0,
  PRIMARY KEY (guid)
);

CREATE TABLE tab_promocao (
  guid INT NOT NULL AUTO_INCREMENT,
  ativo INT NOT NULL DEFAULT 0,
  titulo VARCHAR(250) NOT NULL,
  descricao TEXT,
  guid_imagem INT,
  valor FLOAT,
  percentual INT NOT NULL DEFAULT 0,
  PRIMARY KEY (guid)
);

CREATE TABLE tab_locais (
  guid INT NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(250),
  taxa FLOAT,
  PRIMARY KEY (guid)
);

CREATE TABLE cfg_configuracao (
  guid INT NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(250),
  propriedade VARCHAR(150),
  valor INT NOT NULL,
  PRIMARY KEY (guid)
);

CREATE TABLE sys_key (
  guid INT NOT NULL AUTO_INCREMENT,
  chave VARCHAR(32) NOT NULL,
  PRIMARY KEY (guid)
);
